import React, { Component } from 'react';

const crossCorners = { "0": 8, "2": 6, "6": 2, "8": 6 };

export default class Tictac extends Component {

    constructor(props) {
        super(props);
        this.corners = [0, 2, 6, 8];
        this.state = {
            turn: 'O',
            X: [],
            O: [],
            robot: true
        };
        this.X = (<img alt="" src="x.png" width='25px' height='25px' />);
        this.O = (<img alt="" src="circle.png" width='25px' height='25px' />);
        this.getRows = this.getRows.bind(this);
        this.onClick = this.onClick.bind(this);
        this.tryToWin = this.tryToWin.bind(this);
        this.initFirstRobot = this.initFirstRobot.bind(this);
        this.winner = [
            [0, 1, 2], [3, 4, 5], [6, 7, 8],
            [0, 3, 6], [2, 5, 8], [0, 4, 8],
            [2, 4, 6], [1, 4, 7]
        ];
    }

    componentDidMount() {
        this.initFirstRobot();
    }

    initFirstRobot() {
        this.setState({ X: [this.corners[Math.floor(Math.random() * this.corners.length)]] })
    }

    validateInput(key) {
        return !this.state.X.includes(parseInt(key)) && !this.state.O.includes(parseInt(key));
    }

    initSecondtRobot() {
        let value = crossCorners[this.state.X[0].toString()];
        if (this.state.O.includes(value)) {
            for (let key in crossCorners) {
                if (this.validateInput(key)) {
                    value = parseInt(key);
                }
            }
        }
        this.setState({
            X: [...this.state.X, value],
            robot: !this.state.robot,
            turn: this.state.turn === 'X' ? 'O' : 'X'
        });
    }

    winAndBlock() {
        const { turn } = this.state;
        const win = this.tryToWin(this.state[turn]);
        const turnNext = turn === 'X' ? 'O' : 'X';

        if (win) {
            this.setState({
                [turn]: [...this.state[turn], win]
            });
            return true;
        } else {

            const blockWin = this.tryToWin(this.state[turnNext]);

            if (blockWin) {
                this.setState({
                    [turn]: [...this.state[turn], blockWin],
                    robot: !this.state.robot,
                    turn: this.state.turn === 'X' ? 'O' : 'X'
                });
                return true;
            }
        }
        return false;
    }

    setNextValue(nextValue){
        const { turn } = this.state;
        this.setState({
            [turn]: [...this.state[turn], nextValue],
            robot: !this.state.robot,
            turn: this.state.turn === 'X' ? 'O' : 'X'
        });
    }

    initThirdRobot() {
        if (!this.winAndBlock()) {
            let array = [];
            for (let key in crossCorners) {
                if (this.validateInput(key)) {
                    array.push(parseInt(key));
                }
            }
            this.setNextValue( array[Math.floor(Math.random() * array.length)]);  
        }
    }

    initFourthRobot() {
        if (!this.winAndBlock()) {
            let array = [];
            for (let i = 0; i < 9; i++) {
                if (this.validateInput(i)) {
                    array.push(i);
                }
            }
            this.setNextValue(array[Math.floor(Math.random() * array.length)]);
        }
    }

    tryToWin(moves) {
        let selectValue = [];
        this.winner.forEach(list => {
            let count = 0;
            let value = -1;

            list.forEach(e => {
                if (moves.includes(e)) {
                    count++;
                } else {
                    value = e;
                }
            });

            if (count === 2) {
                selectValue.push(value)
            }
        });

        return selectValue.reduce((init, e) => {
            return init ? init : this.validateInput(e) ? e : undefined;
        }, undefined);
    }

    onClick(event) {
        const { turn } = this.state;
        let selected = parseInt(event.target.id);
        if (turn === 'O' && this.validateInput(selected) && event.target.id) {
            let moves = [...this.state[turn]];
            moves.push(selected)
            moves.sort();
            this.setState({
                turn: turn === 'X' ? 'O' : 'X',
                [turn]: moves,
                winner: this.checkWinner(moves) ? turn : undefined,
                robot: !this.state.robot
            }, () => {
                const { turn } = this.state;
                setTimeout(() => {
                    switch (this.state[turn].length) {
                        case 1:
                            this.initSecondtRobot();
                            break;
                        case 2:
                            this.initThirdRobot();
                            break;
                        case 3:
                        case 4:
                            this.initFourthRobot();
                            break;
                        default:
                            break;
                    }
                }, Math.floor(Math.random() * 3000));
            });
        }
    }

    checkWinner(moves) {
        return this.winner.reduce((init, list) => {
            if (init || (moves.includes(list[0]) && moves.includes(list[1]) && moves.includes(list[2]))) {
                return true;
            }
            return false;
        }, false);
    }

    validateWin() {
        const { turn } = this.state;
        return this.checkWinner(this.state[turn]) ? turn : undefined;
    }

    componentDidUpdate() {
        const winner = this.validateWin();
        const total = this.state.X.length + this.state.O.length;
        if (winner || total === 9) {
            setTimeout(() => {
                const reset = {
                    turn: 'O',
                    X: [this.corners[Math.floor(Math.random() * this.corners.length)]],
                    O: [],
                    robot: true
                }; 
                if ((winner && window.confirm(winner + " WON! Do you wanna keep playing?")) ||
                    (total === 9 && window.confirm("It is a tie, do you wanna keep playing?"))) {
                    this.setState({ ...reset });
                }
            }, 1000);
        }
    }

    getRows() {
        let index = 0;
        let rows = [];
        while (index < 9) {
            let fields = [];
            for (let count = 0; count < 3; count++) {
                if (this.state.X.includes(index)) {
                    fields.push(<td key={index} id={index} onClick={this.onClick}>{this.X}</td>);
                } else if (this.state.O.includes(index)) {
                    fields.push(<td key={index} id={index} onClick={this.onClick}>{this.O}</td>);
                } else {
                    fields.push(<td key={index} id={index} onClick={this.onClick}></td>);
                }
                index++;
            }
            rows.push(<tr>{fields}</tr>)
        }
        return rows;
    }

    render() {
        return (
            <table key="table">
                <tbody>
                    {this.getRows()}
                </tbody>
            </table>
        );
    }
}
